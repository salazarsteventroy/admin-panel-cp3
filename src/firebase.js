// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyByuOxRU-bimMfU8ye3nQTLtnuURBI_S4Y",
  authDomain: "ecommercetroy-ff7de.firebaseapp.com",
  projectId: "ecommercetroy-ff7de",
  storageBucket: "ecommercetroy-ff7de.appspot.com",
  messagingSenderId: "508416849768",
  appId: "1:508416849768:web:887229e1f2ae7f910fc8dd",
  measurementId: "G-97T3XSYL1T"
};


// Initialize Firebase
const app = initializeApp(firebaseConfig);

export default app;
